import pngLogo from '!!file-loader!../../images/react_logo.png';
import svgLogo from '!!file-loader!../../svg/react_logo.svg';

import React from 'react';
import { connect } from 'react-redux';
import store from 'store';
import classNames from 'classnames';
import responsive from 'stylesResponsive.css';
import st from './sample.css';

@connect((store) => ({}))
export default class Sample extends React.Component {

    constructor(props) {
        super(props);
    }

    render() {
        const stImages = classNames(responsive.col_6, responsive.col_m_6);
        return (
            <div className={ responsive.row } id="Sample">
                <fieldset className={ st.content }>
                    <legend>Sample</legend>
                    <div className={ st.text }>
                        <p>I&apos;m Sample component!</p>
                        <p>~/src/components/Sample/Sample.js</p>
                    </div>
                    <div className={ stImages }>
                        <img src={ pngLogo } className={ st.image } alt="Logo png Format." />
                        <p>PNG Format.</p>
                    </div>
                    <div className={ stImages }>
                        <img src={ svgLogo } className={ st.image } alt="Logo svg Format." />
                        <p>SVG Format.</p>
                    </div>
                </fieldset>
            </div>);
    }
  }
