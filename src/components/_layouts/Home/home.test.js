import store from 'store';
import React from 'react';
import { Provider } from 'react-redux';
import { shallow, mount, render } from 'enzyme';
import renderer from 'react-test-renderer';
import Home from './Home';
import * as actions from './homeActions';
import reducer from './homeReducer';
import { sample, API } from 'api';
import Conf from 'conf';

describe('Home', ()=> {
    describe('visual', () => {
        // Variables
        let enzymeComponent = null;
        let tree = null;
        beforeAll(() => {
            const element = <Provider store={ store }><Home /></Provider>;
            enzymeComponent = mount(element);
            tree = renderer.create(element).toJSON();
            expect(tree).toMatchSnapshot();
        });
        test('Render text', () => {
            const str = 'Home';
            expect(enzymeComponent.find('#Home').text()).toContain(str);
        });
    });

    describe('reducers', () => {
        // Variables available for all tests.
        let reducerState = store.getState().Home;
        test('HOME_ADD_ERROR', () => {
            const payload = { value: 1, 'id_test': 'test' };
            const response = reducer(reducerState, { type: 'HOME_ADD_ERROR', payload });
            expect(response.error).toEqual(payload);
        });
        test('HOME_REMOVE_ERROR', () => {
            const response = reducer(reducerState, { type: 'HOME_REMOVE_ERROR'});
            expect(response.error).toEqual(false);
        });
        test('HOME_ADD_NOTIFICATION', () => {
            const payload = 'String advertisement...';
            const response = reducer(reducerState, { type: 'HOME_ADD_NOTIFICATION', payload });
            expect(response.notifications).toEqual([payload]);
        });
        test('HOME_REMOVE_NOTIFICATIONS', () => {
            const response = reducer(reducerState, { type: 'HOME_REMOVE_NOTIFICATIONS' });
            expect(response.notifications).toEqual([]);
        });
    });

    describe('actions', () => {
        test('addError(text)', (done) => {
            const str = 'string error.';
            store.subscribe(() => {
                let props = store.getState().Home;
                try {
                    expect(props.error).toEqual(str);
                } catch(e) {
                    throw Error(e);
                    console.log(e);
                }
                done();
            });
            store.dispatch(actions.addError(str));
        });
        test('addNotification(str)', (done) => {
            const str = 'string notification.';
            store.subscribe(() => {
                let props = store.getState().Home;
                try {
                    expect(props.notifications.reverse()[0]).toEqual(str);
                } catch(e) {
                    throw Error(e);
                    console.log(e);
                }
                done();
            });
            store.dispatch(actions.addNotification(str));
        });
        test('removeError()', (done) => {
            store.subscribe(() => {
                let props = store.getState().Home;
                try {
                    expect(props.error).toEqual(false);
                } catch(e) {
                    throw Error(e);
                    console.log(e);
                }
                done();
            });
            store.dispatch(actions.removeError());
        });
        test('removeNotification()', (done) => {
            store.subscribe(() => {
                let props = store.getState().Home;
                try {
                    expect(props.notifications).toEqual([]);
                } catch(e) {
                    throw Error(e);
                    console.log(e);
                }
                done();
            });
            store.dispatch(actions.removeNotification());
        });
        test('loading(true)', (done) => {
            store.subscribe(() => {
                let props = store.getState().Loading;
                try {
                    expect(props.active).toEqual(true);
                } catch(e) {
                    throw Error(e);
                    console.log(e);
                }
                done();
            });
            store.dispatch(actions.loading(true));
        });
        test('loading(false)', (done) => {
            store.subscribe(() => {
                let props = store.getState().Loading;
                try {
                    expect(props.active).toEqual(false);
                } catch(e) {
                    throw Error(e);
                    console.log(e);
                }
                done();
            });
            store.dispatch(actions.loading(false));
        });
    });
});
