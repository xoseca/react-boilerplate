import React from 'react';
import { connect } from 'react-redux';
import store from 'store';
import classNames from 'classnames';
import { Sample } from 'components';
import { Loading } from 'helpers';
import responsive from 'stylesResponsive.css';
import * as actions from './homeActions';
import st from './home.css';

@connect((store) => ({
    error: store.Home.error,
    notifications: store.Home.notifications,
}))
export default class Home extends React.Component {

    constructor(props) {
        super(props);
        this.showNotifications = this.showNotifications.bind(this);
        this.showErrors = this.showErrors.bind(this);
    }

    componentWillMount() {
        // Activate helper <Loading />.
        this.props.dispatch(actions.loading(true));
        // Send notifications.
        this.props.dispatch(actions.addNotification('1 String sample text!'));
        this.props.dispatch(actions.addNotification('2 String sample text!'));
        // Send error.
        this.props.dispatch(actions.addError('Some advertisement...'));
    }

    showNotifications() {
        if (this.props.notifications) {
            return this.props.notifications.map((row, pos) => {
                return <div key={ pos } className={ st.notification }>{ row }</div>;
            });
        }
        return null;
    }

    showErrors() {
        if (this.props.error) {
            return <div className={ st.error }>Error: { this.props.error }</div>;
        }
        return null;
    }

    render() {
        const laterals = classNames(responsive.col_2, responsive.col_m_2);
        const center = classNames(responsive.col_8, responsive.col_m_8, st.center);
        return (
            <div className={ responsive.row } id="Home">
                <div className={ laterals } />
                <div className={ center }>
                    <fieldset>
                        <legend>Layout Home</legend>
                        {/* Self function interactions */}
                        { this.showNotifications() }
                        { this.showErrors() }
                        {/* Component */}
                        <Sample />
                        {/* Helper */}
                        <Loading />
                    </fieldset>
                </div>
                <div className={ laterals } />
            </div>);
    }
}
