const base = require('../base.json');
const settings = {
    debug: true,
};

module.exports = Object.assign({}, base, settings);
