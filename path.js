const PATH     = require('path');
const NODE_ENV = process.env.NODE_ENV || 'production';

const Path  = module.exports = {};
Path.root   = __dirname;
Path.conf   = PATH.join(Path.root, 'conf', NODE_ENV);
Path.dist   = PATH.join(Path.root, 'dist');
Path.src    = PATH.join(Path.root, 'src');

Path.api    = PATH.join(Path.src, 'api');
Path.store  = PATH.join(Path.src, 'store');
Path.styles = PATH.join(Path.src, 'styles');
Path.images = PATH.join(Path.src, 'images');
Path.reducers    = PATH.join(Path.src, 'reducers');
Path.components  = PATH.join(Path.src, 'components');

Path.app         = PATH.join(Path.components, 'App.js');
Path.layouts     = PATH.join(Path.components, '_layouts');
Path.helpers     = PATH.join(Path.components, '_helpers');
Path.styles_base = PATH.join(Path.styles, 'base.css');
Path.styles_responsive = PATH.join(Path.styles, 'responsive.css');

Path.node_modules  = PATH.join(Path.root, 'node_modules');
Path.dist_assets   = PATH.join(Path.dist, 'assets');
